import React, { Component } from 'react';
import { connect } from 'react-redux';
import {postLink} from "../../store/actions";

import './MainContainer.css';

class MainContainer extends Component {

    state = {
        originalUrl: '',
    };

    getLink = (event) => {
        this.setState({originalUrl: event.target.value});
    };

    onSubmitHandler = (event) => {
        event.preventDefault();
        this.props.postLink(this.state);
        this.setState({originalUrl: ''});
    };

    render() {
        return (
            <div className="container">
                <form className="form" action="submit" onSubmit={this.onSubmitHandler}>
                    <label htmlFor="link">Enter your link</label>
                    <input type="text"
                           name="link"
                           required
                           onChange={this.getLink}
                    />
                    {this.props.shortUrl ? <div className="link">
                        <h4>Here is your short link</h4>
                        <a href={this.props.shortUrl} target="blank">{this.props.shortUrl}</a>
                    </div> : null}
                    <button type="submit">Shorten</button>
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        shortUrl: state.shortUrl
    }
};

const mapDispatchToProps = dispatch => {
    return {
        postLink: (data) => dispatch(postLink(data))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MainContainer);