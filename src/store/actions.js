import axios from '../axios-links';

export const FETCH_LINK = "FETCH_LINK";

export const fetchLinkSuccess = data => {
    return {type: FETCH_LINK, data}
};


export const postLink = (data) => {
    return (dispatch) => {
        return axios.post('/links', data).then(response => {
            dispatch(fetchLinkSuccess(response.data))
        })
    }
};
