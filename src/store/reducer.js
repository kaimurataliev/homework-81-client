import * as actions from './actions';

const initialState = {
    shortUrl: '',
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actions.FETCH_LINK:
            return {...state, shortUrl: action.data};

        default:
            return state;
    }
};

export default reducer;
